package com.appnomic.appsone.controlcenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AppsoneControlcenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppsoneControlcenterApplication.class, args);
	}

}
