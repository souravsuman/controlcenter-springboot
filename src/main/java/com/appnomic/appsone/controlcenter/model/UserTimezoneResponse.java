package com.appnomic.appsone.controlcenter.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserTimezoneResponse {
    private long offset;
    private String offsetName;
}
