package com.appnomic.appsone.controlcenter.model;

import com.appnomic.appsone.controlcenter.beans.UserNotificationPreferenceBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author krithikak
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationDetails {

    private int applicationId;
    private String applicationName;
    private String applicationIdentifier;
    private int status;

    private List<UserNotificationPreferenceBean> notificationPreferences;
}
