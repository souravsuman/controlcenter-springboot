package com.appnomic.appsone.controlcenter.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties("applications")
public class User {

    private String userId;
    private String username;
    private String firstName;
    private String lastName;
    private int emailEnabled;
    private int smsEnabled;
    private String email;
    private String contactNumber;
    private boolean enabled;
    private List<ApplicationDetails> applicationDetails;
    private List<String> applications;
    private UserTimezoneResponse timezoneDetail;
}
