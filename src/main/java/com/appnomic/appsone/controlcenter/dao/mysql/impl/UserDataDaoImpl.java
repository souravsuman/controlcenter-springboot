package com.appnomic.appsone.controlcenter.dao.mysql.impl;


import com.appnomic.appsone.controlcenter.dao.mysql.UserDataDao;
import com.appnomic.appsone.controlcenter.exceptions.ControlCenterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

//Alias of UserDataService
@Repository
public class UserDataDaoImpl implements UserDataDao {
    private final Logger logger = LoggerFactory.getLogger(UserDataDaoImpl.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String ERROR_MESSAGE = "Exception while getting Users. {} ";
    private final String ERROR_ADD_USER = "Error while adding user : user_attributes or user_access_details";
    private final String ERROR_UPDATE_USER = "Error while updating user : user_attributes or user_access_details";

    private static final String DELETE_USER_FROM_USERID_QUERY = "delete from  user_attributes where user_identifier = ?";
    private static final String DELETE_USER_FROM_ACCESSDETAILS_QUERY = "delete from user_access_details where user_identifier = ?";

//    public String getSetup() {
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//        String setup = Constants.SETUP_KEYCLOAK;
//        try {
//            setup = userDao.getSetup();
//
//            if (setup == null) {
//                return Constants.SETUP_KEYCLOAK;
//            }
//
//        } catch (Exception e) {
//            logger.error(ERROR_MESSAGE, e.getMessage(), e);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//        return setup;
//    }
//
//    public String getTemporaryPassword() {
//        logger.trace("Method Invoked : UserDataService/getTemporaryPassword");
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//
//        try {
//            return userDao.getTemporaryPassword();
//
//        } catch (Exception e) {
//            logger.error("Unable to fetch default credentials.", e);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//        return null;
//    }
//
//    public UserAttributesBean getUserAttributes(String userId) throws ControlCenterException {
//
//        logger.debug("Invoking get user attributes");
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//        try {
//            return userDao.getUserAttributes(userId);
//        } catch (Exception e) {
//            String ERROR_GET_USER_ATTRIBUTES = "Error in getting user attributes from DB";
//            logger.error(ERROR_GET_USER_ATTRIBUTES + ". Reason: {}", e.getMessage(), e);
//            throw new ControlCenterException(ERROR_GET_USER_ATTRIBUTES);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//    }
    @Transactional
    public void deleteUserAttributesAndAccessDetails(String userId) throws ControlCenterException {
        try {
            logger.debug("DELETE user attributes and user access details row");
            jdbcTemplate.update(DELETE_USER_FROM_USERID_QUERY, new Object[] {userId});
            jdbcTemplate.update(DELETE_USER_FROM_ACCESSDETAILS_QUERY, new Object[] {userId});
        } catch (Exception e) {
            String ERROR_DELETE_USER_ATTRIBUTES = "Error in deleting user_attributes or user_access_details";
            logger.error(ERROR_DELETE_USER_ATTRIBUTES + ". Reason: {}", e.getMessage(), e);
            throw new ControlCenterException(ERROR_DELETE_USER_ATTRIBUTES);
        }
    }

//    public void addUserAttributes(UserAttributesBean user, Handle handle) throws ControlCenterException {
//        UserDao userDao = null;
//        try {
//            logger.debug("Add user in user_attributes.");
//            userDao = getDaoConnection(handle, UserDao.class);
//            userDao.addUserAttributes(user);
//        } catch (Exception e) {
//            logger.error(ERROR_ADD_USER, e);
//            throw new ControlCenterException(ERROR_ADD_USER);
//        } finally {
//            closeDaoConnection(handle, userDao);
//        }
//    }

//    public void addUserAccessDetails(UserAccessDetails user, Handle handle) throws ControlCenterException {
//        UserDao userDao = null;
//        try {
//            logger.debug("Add user in user_access_details.");
//            userDao = getDaoConnection(handle, UserDao.class);
//            userDao.addUserAccessDetails(user);
//        } catch (Exception e) {
//            logger.error(ERROR_ADD_USER, e);
//            throw new ControlCenterException(ERROR_ADD_USER);
//        } finally {
//            closeDaoConnection(handle, userDao);
//        }
//    }

//    public void updateUserAttributes(UserAttributesBean user, Handle handle) throws ControlCenterException {
//        UserDao userDao = null;
//        try {
//            logger.debug("Update user in user_attributes.");
//            userDao = getDaoConnection(handle, UserDao.class);
//            userDao.updateUserAttributes(user);
//        } catch (Exception e) {
//            logger.error(ERROR_UPDATE_USER, e);
//            throw new ControlCenterException(ERROR_UPDATE_USER);
//        } finally {
//            closeDaoConnection(handle, userDao);
//        }
//    }

//    public void updateStatusForUser(String identifier, int status, String updatedTime, String userId, Handle handle) throws ControlCenterException {
//        UserDao userDao = null;
//        try {
//            logger.debug("Update user status in user_attributes.");
//            userDao = getDaoConnection(handle, UserDao.class);
//            userDao.updateStatusForUser(identifier, userId, status, updatedTime);
//        } catch (Exception e) {
//            logger.error(ERROR_UPDATE_USER, e);
//            throw new ControlCenterException(ERROR_UPDATE_USER);
//        } finally {
//            closeDaoConnection(handle, userDao);
//        }
//    }
//
//    public void updateUserStatusToInactive(String userIdentifier, String superUserIdentifier, Handle handle) throws ControlCenterException {
//        UserDao userDao = null;
//        try {
//            logger.debug("Update user status in user_attributes.");
//            userDao = getDaoConnection(handle, UserDao.class);
//            userDao.updateUserStatus(userIdentifier,superUserIdentifier);
//        } catch (Exception e) {
//            logger.error(ERROR_UPDATE_USER, e);
//            throw new ControlCenterException(ERROR_UPDATE_USER);
//        } finally {
//            closeDaoConnection(handle, userDao);
//        }
//    }
//
//    public void updateUserLastLoginTime(String userIdentifier, String lastLoginTime, Handle handle) throws ControlCenterException {
//        UserDao userDao = null;
//        try {
//            logger.debug("Update user last login time column in user_attributes.");
//            userDao = getDaoConnection(handle, UserDao.class);
//            userDao.updateUserLastLoginTime(userIdentifier, lastLoginTime);
//        } catch (Exception e) {
//            logger.error(ERROR_UPDATE_USER, e);
//            throw new ControlCenterException(ERROR_UPDATE_USER);
//        } finally {
//            closeDaoConnection(handle, userDao);
//        }
//    }
//    public void updateUserAccessDetails(UserAccessDetails user, Handle handle) throws ControlCenterException {
//        UserDao userDao = null;
//        try {
//            logger.debug("Update user in user_access_details.");
//            userDao = getDaoConnection(handle, UserDao.class);
//            userDao.updateUserAccessDetails(user);
//        } catch (Exception e) {
//            logger.error(ERROR_UPDATE_USER, e);
//            throw new ControlCenterException(ERROR_UPDATE_USER);
//        } finally {
//            closeDaoConnection(handle, userDao);
//        }
//    }
//
//    public List<UserDetailsBean> getUsers() {
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//        try {
//            List<UserDetailsBean> users = userDao.getUsers();
//            if (users == null) {
//                return Collections.emptyList();
//            }
//            return users;
//        } catch (Exception e) {
//            logger.error(ERROR_MESSAGE, e.getMessage(), e);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//        return Collections.emptyList();
//    }
//
//    public UserInfoBean getUserDetails(String userName) {
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//
//        try {
//            return userDao.getUserDetails(userName);
//        } catch (Exception e) {
//            logger.error(ERROR_MESSAGE, e.getMessage(), e);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//
//        return null;
//    }
//
//    public boolean adEditStatusKeycloak() throws ControlCenterException {
//        UserDao userDao = null;
//        try {
//            userDao = getDaoConnection(null, UserDao.class);
//            return userDao.getAdEditStatusKeycloak() > 0;
//        } catch (Exception e) {
//            logger.error("Unable to fetch status : adEditStatusKeycloak.", e);
//            throw new ControlCenterException("Unable to fetch status : adEditStatusKeycloak.");
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//    }
//
//
//    // This method is provided for testing purposes only.
//    public void updateSetup(String setup) {
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//        try {
//            userDao.updateSetup(setup);
//
//        } catch (Exception e) {
//            logger.error("Unable to update the setup. {}", e.getMessage(), e);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//    }
//
//    public List<UserInfoBean> getNotificationUsers() {
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//
//        try {
//            return userDao.getNotificationUsers();
//        } catch (Exception e) {
//            logger.error(ERROR_MESSAGE, e.getMessage(), e);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//
//        return Collections.emptyList();
//    }
//
//    public NotificationChoiceBean getUserNotificationChoice(String applicableUserId) {
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//        try {
//            return userDao.getUserNotificationChoice(applicableUserId);
//        } catch (Exception e) {
//            logger.error("Error occurred in getUserNotificationChoice() applicableUserId: {}", applicableUserId);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//        return null;
//    }
//
//    public List<UserInfoBean> getActiveUsers() {
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//
//        try {
//            return userDao.getActiveUsers();
//        } catch (Exception e) {
//            logger.error(ERROR_MESSAGE, e.getMessage(), e);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//
//        return Collections.emptyList();
//    }
//    public UserInfoBean getSuperAdmin() {
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//
//        try {
//            return userDao.getSuperAdmin();
//        } catch (Exception e) {
//            logger.error(ERROR_MESSAGE, e.getMessage(), e);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//
//        return null;
//    }
//
//    public UserAttributesBean getUserAttributesByContact(String contactNo) throws ControlCenterException {
//
//        logger.debug("Invoking get user attributes");
//        UserDao userDao = getDaoConnection(null, UserDao.class);
//        try {
//            return userDao.getUserAttributesByContact(contactNo);
//        } catch (Exception e) {
//            String ERROR_GET_USER_ATTRIBUTES = "Error in getting user attributes from DB";
//            logger.error(ERROR_GET_USER_ATTRIBUTES + ". Reason: {}", e.getMessage(), e);
//            throw new ControlCenterException(ERROR_GET_USER_ATTRIBUTES);
//        } finally {
//            closeDaoConnection(null, userDao);
//        }
//    }
//
//    public void updateUserOptIn(OptInRequestPojo bean, Handle handle) {
//        UserDao userDao = null;
//        try {
//            logger.debug("Update whatsapp opt-in in user_attributes.");
//            userDao = getDaoConnection(handle, UserDao.class);
//            userDao.updateUserWhatsappOptIn(bean.getOptInStatus(), bean.getOptInLastRequestTimeStr(), bean.getUserAttributesBean().getUserIdentifier());
//        } catch (Exception e) {
//            logger.error(ERROR_UPDATE_USER, e);
//        } finally {
//            closeDaoConnection(handle, userDao);
//        }
//    }

}
