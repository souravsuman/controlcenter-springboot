package com.appnomic.appsone.controlcenter.dao.mysql;

import com.appnomic.appsone.controlcenter.exceptions.ControlCenterException;

public interface UserDataDao {

    public void deleteUserAttributesAndAccessDetails(String userId) throws ControlCenterException;
}
