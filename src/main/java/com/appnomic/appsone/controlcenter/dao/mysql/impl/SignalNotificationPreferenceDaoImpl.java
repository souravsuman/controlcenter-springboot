package com.appnomic.appsone.controlcenter.dao.mysql.impl;

import com.appnomic.appsone.controlcenter.dao.mysql.SignalNotifiactionPreferenceDao;
import com.appnomic.appsone.controlcenter.exceptions.ControlCenterException;
import com.appnomic.appsone.controlcenter.service.NotificationPreferencesDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SignalNotificationPreferenceDaoImpl implements SignalNotifiactionPreferenceDao {

    private static final Logger logger = LoggerFactory.getLogger(SignalNotificationPreferenceDaoImpl.class);
    private static  final String REMOVE_NOTIFICATION_DETAIL_USER_QUERY = "DELETE FROM user_notifications_details where applicable_user_id = ?";
    private static  final String REMOVE_FORENSIC_NOTIFICATION_MAP_QUERY = "DELETE FROM user_forensic_notification_mapping where applicable_user_id = ?";
    private static  final String REMOVE_USER_NOTIFICATION_MAP_QUERY = "DELETE from user_notification_mapping where applicable_user_id = ?";

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void removeNotificationDetailsForUser(String userId) throws ControlCenterException {
        try {
            jdbcTemplate.update(REMOVE_NOTIFICATION_DETAIL_USER_QUERY, new Object[] {userId});
        }catch (Exception e){
            logger.error("Error in deleting notification for user", e);
        }
    }

    @Override
    public void removeForensicNotificationPreferencesForUser(String userId) throws ControlCenterException {
        try {
            jdbcTemplate.update(REMOVE_FORENSIC_NOTIFICATION_MAP_QUERY, new Object[] {userId});
        }catch (Exception e){
            logger.error("Error in deleting forensic notification for user {}", userId, e);
        }
    }

    @Override
    public void removeUserNotificationPreferencesForUser(String userId) throws ControlCenterException {
        try {
            jdbcTemplate.update(REMOVE_USER_NOTIFICATION_MAP_QUERY, new Object[] {userId});
        }catch (Exception e){
            logger.error("Error in deleting forensic notification for user {}", userId, e);
        }
    }
}
