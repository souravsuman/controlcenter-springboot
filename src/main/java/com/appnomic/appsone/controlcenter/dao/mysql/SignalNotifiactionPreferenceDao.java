package com.appnomic.appsone.controlcenter.dao.mysql;

import com.appnomic.appsone.controlcenter.exceptions.ControlCenterException;

public interface SignalNotifiactionPreferenceDao {
    void removeNotificationDetailsForUser(String userId) throws ControlCenterException;
    void removeForensicNotificationPreferencesForUser(String userId) throws ControlCenterException;
    void removeUserNotificationPreferencesForUser(String userId) throws ControlCenterException;
}
