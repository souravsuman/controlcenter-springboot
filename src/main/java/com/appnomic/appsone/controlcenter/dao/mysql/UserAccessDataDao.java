package com.appnomic.appsone.controlcenter.dao.mysql;

import com.appnomic.appsone.controlcenter.beans.UserBean;
import com.appnomic.appsone.controlcenter.exceptions.ControlCenterException;

import java.util.List;

public interface UserAccessDataDao {
    List<String> getUserIdentifiers() throws ControlCenterException;
    List<String> getKeycloakUserIdentifiers() throws ControlCenterException;
    String getUsernameFromIdentifier(String userId);
    List<UserBean> getUserDetailsFromKeycloak() throws ControlCenterException;
}
