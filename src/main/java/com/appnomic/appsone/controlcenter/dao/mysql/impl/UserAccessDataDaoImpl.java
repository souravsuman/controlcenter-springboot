package com.appnomic.appsone.controlcenter.dao.mysql.impl;

import com.appnomic.appsone.controlcenter.beans.UserBean;
import com.appnomic.appsone.controlcenter.dao.mysql.UserAccessDataDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.appnomic.appsone.controlcenter.exceptions.ControlCenterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserAccessDataDaoImpl implements UserAccessDataDao {

    private static final String KEYCLOAK_USER_IDENTIFIER_QUERY = "select id from appsonekeycloak.USER_ENTITY";
    private static final String USER_IDENTIFIER_QUERY = "select ua.user_identifier from user_attributes ua,user_access_details uad where uad.user_identifier=ua.user_identifier";
    private static final String USERNAME_BASED_IDENTIFIER_QUERY = "select username from user_attributes where user_identifier= ?";
    private static final String USER_DETAILS_FROM_KEYCLOAK_QUERY = "select first_name firstName, last_name lastName, email, id, username, enabled from appsonekeycloak.USER_ENTITY";

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(UserAccessDataDaoImpl.class);

    @Override
    public List<String> getUserIdentifiers() throws ControlCenterException {
        try {
            return jdbcTemplate.queryForList(USER_IDENTIFIER_QUERY, String.class);
        } catch (Exception e) {
            logger.error("Error while getting user attribute details from DB", e);
            throw new ControlCenterException("Error in fetching user attribute details");
        }
    }

    @Override
    public List<String> getKeycloakUserIdentifiers() throws ControlCenterException {
        try {
            return jdbcTemplate.queryForList(KEYCLOAK_USER_IDENTIFIER_QUERY, String.class);
        } catch (Exception e) {
            logger.error("Error while getting keycloak user identifiers from DB", e);
            throw new ControlCenterException("Error in fetching keycloak user identifiers.");
        }
    }

    @Override
    public String getUsernameFromIdentifier(String userId) {
        try {
            return (String) this.jdbcTemplate.queryForObject(USERNAME_BASED_IDENTIFIER_QUERY, new Object[]{ userId }, String.class);
        } catch (Exception e) {
            logger.error("Error while getting user identifier", e);
            return null;
        }
    }

    @Override
    public List<UserBean> getUserDetailsFromKeycloak() throws ControlCenterException {
        try {
            return jdbcTemplate.query(USER_DETAILS_FROM_KEYCLOAK_QUERY, new BeanPropertyRowMapper(UserBean.class));
        } catch (Exception e) {
            logger.error("Error while getting user identifier", e);
            throw new ControlCenterException("Error while getting user identifier");
        }
    }
}
