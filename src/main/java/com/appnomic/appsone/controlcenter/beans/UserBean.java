package com.appnomic.appsone.controlcenter.beans;

import com.appnomic.appsone.controlcenter.model.User;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@Data
public class UserBean {

    private static final Logger logger = LoggerFactory.getLogger(UserBean.class);
    private static final String ATTRIBUTE_ACCOUNTS = "";
    private static final String ATTRIBUTE_CONTACT_NUMBER = "";

    private String id;
    private String username;
    private String email;
    private boolean enabled;
    private boolean emailVerified;
    private Map<String, Object> attributes;
    private Map<String, List<String>> applicationMap;
    private Set<String> accounts;
    private String firstName;
    private String lastName;

    public void setAttributes(Map<String, Object> attributes){

        applicationMap = new HashMap<>();
        if(attributes == null || attributes.isEmpty()
                || !attributes.containsKey(ATTRIBUTE_ACCOUNTS)){
            return;
        }

        List<String> accountList = (List<String>) this.attributes.get(ATTRIBUTE_ACCOUNTS);
        if(accountList.isEmpty()){
            return;
        }
        String[] accountNames = accountList.get(0).split(",");
        this.accounts = new HashSet<>();
        for(String accountName : accountNames){
            this.accounts.add(accountName.trim());
        }

        Set<String> keys = attributes.keySet();

        for(String key : keys){
            if(!key.endsWith(".Application")){
                continue;
            }
            List<String> applicationList = (List<String>) this.attributes.get(key);
            if(!applicationList.isEmpty()){
                String[] applicationNames = applicationList.get(0).split(",");
                List<String> applications = Arrays.asList(applicationNames);
                this.applicationMap.put(key, applications);
            }
        }
    }

    public User getUser(String accountName){

        if(applicationMap == null){
            setAttributes(this.attributes);
        }

        if(StringUtils.isEmpty(accountName) || this.accounts == null
                || !(accounts.contains(accountName) || accounts.contains("*"))){
            return null;
        }
        User user = new User();
        user.setUserId(this.id);
        user.setUsername(this.username);
        user.setFirstName(this.firstName);
        user.setLastName(this.lastName);
        user.setEmail(this.email);
        user.setEnabled(this.enabled);

        if(attributes.containsKey(ATTRIBUTE_CONTACT_NUMBER)){
            List<String> contacts = (List<String>)attributes.get(ATTRIBUTE_CONTACT_NUMBER);
            if(!contacts.isEmpty()) {
                user.setContactNumber(contacts.get(0));
            }
        }

        return user;
    }

    public List<String> getApplicationNamesForUser(String accountName) {
        if(applicationMap == null){
            setAttributes(this.attributes);
        }

        if ("7640123a-fbde-4fe5-9812-581cd1e3a9c1".equals(id.trim())) {
            List<String> allApps = new ArrayList<>();
            allApps.add("*");
            return allApps;
        }

        List<String> apps = this.applicationMap.get(accountName+".Application");

        if(null == apps) {
            return new ArrayList<>();
        }

        return apps;
    }
}
