package com.appnomic.appsone.controlcenter.service;

import com.appnomic.appsone.controlcenter.dao.mysql.SignalNotifiactionPreferenceDao;
import com.appnomic.appsone.controlcenter.exceptions.ControlCenterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class NotificationPreferencesDataService {

    @Autowired
    private SignalNotifiactionPreferenceDao signalNotifiactionPreferenceDao;

    private static final Logger logger = LoggerFactory.getLogger(NotificationPreferencesDataService.class);

//    public static List<UserNotificationPreferenceBean> getNotificationPreferencesForUser(int accountId) {
//        SignalNotificationPreferenceDao notifPreferenceDao = null;
//        try {
//            notifPreferenceDao =
//                    MySQLConnectionManager.getInstance().getHandle().open(SignalNotificationPreferenceDao.class);
//            return notifPreferenceDao.getNotificationPreferenceForUser(accountId);
//
//        } catch (Exception e) {
//            logger.error("Exception encountered while fetching user notification preferences. Reason: {}", e.getMessage(), e);
//            return new ArrayList<>();
//        } finally {
//            if (notifPreferenceDao != null) {
//                MySQLConnectionManager.getInstance().close(notifPreferenceDao);
//            }
//        }
//    }

    @Transactional
    public void removeUserNotificationPreferencesForUser(String userId) throws ControlCenterException {
        try {
            signalNotifiactionPreferenceDao.removeUserNotificationPreferencesForUser(userId);
        } catch (Exception e) {
            logger.error("Error while deleting user notification and user notification details data from DB. Reason :{}", e.getMessage(), e);
            throw new ControlCenterException("Error while deleting user notification and user notification details data from DB");
        }
    }
    @Transactional
    public void removeForensicNotificationPreferencesForUser(String userId) throws ControlCenterException {
        try {
            signalNotifiactionPreferenceDao.removeForensicNotificationPreferencesForUser(userId);
        } catch (Exception e) {
            logger.error("Error while deleting user notification and user notification details data from DB. Reason :{}", e.getMessage(), e);
            throw new ControlCenterException("Error while deleting user notification and user notification details data from DB");
        }
    }
    @Transactional
    public void removeNotificationDetailsForUser(String userId) throws ControlCenterException {
        try {
            signalNotifiactionPreferenceDao.removeNotificationDetailsForUser(userId);
        } catch (Exception e) {
            logger.error("Exception encountered while deleting user notification preferences for user [{}]. Reason: {}", userId, e.getMessage(), e);
            throw new ControlCenterException("Error in deleting user notification preferences");
        }
    }

//    public static UserNotificationDetails getEmailAndSmsStatus(String userId, Handle handle) {
//        SignalNotificationPreferenceDao notifPreferenceDao = getSignalNotifPreferenceDao(handle);
//        try {
//            return notifPreferenceDao.getEmailAndSmsStatus(userId);
//        } catch (Exception e) {
//            logger.error("Exception encountered while fetching user notification details. Reason: {}", e.getMessage(), e);
//            return null;
//        } finally {
//            closeDaoConnection(handle, notifPreferenceDao);
//        }
//    }
//
//    public static List<NotificationSettingsBean> getNotificationSettingsForAccount(int accountId) {
//        SignalNotificationPreferenceDao notifPreferenceDao = null;
//        try {
//            notifPreferenceDao = MySQLConnectionManager.getInstance().getHandle().open(SignalNotificationPreferenceDao.class);
//            return notifPreferenceDao.getNotificationSettings(accountId);
//
//        } catch (Exception e) {
//            logger.error("Exception encountered while fetching user notification details. Reason: {}", e.getMessage(), e);
//            return new ArrayList<>();
//
//        } finally {
//            if (notifPreferenceDao != null) {
//                MySQLConnectionManager.getInstance().close(notifPreferenceDao);
//            }
//        }
//    }
//
//    public static int[] addNotificationDetails(List<NotificationBean> notificationBean, Handle handle) throws ControlCenterException {
//
//        SignalNotificationPreferenceDao signalNotificationPreferenceDao = getSignalNotifPreferenceDao(handle);
//        try {
//            return signalNotificationPreferenceDao.addNotificationDetails(notificationBean);
//        } catch (Exception e) {
//            logger.error("Error while inserting user notification mapping from DB. Reason :{}", e.getMessage(), e);
//            throw new ControlCenterException("Error while inserting user notification mapping from DB");
//        } finally {
//            closeDaoConnection(handle, signalNotificationPreferenceDao);
//        }
//    }
//
//    public static void addForensicNotificationConfigurations(List<UserForensicNotificationMappingBean> beans, Handle handle) throws ControlCenterException {
//        SignalNotificationPreferenceDao signalNotificationPreferenceDao = getSignalNotifPreferenceDao(handle);
//        try {
//            signalNotificationPreferenceDao.addForensicNotificationConfigurations(beans);
//        } catch (Exception e) {
//            logger.error("Error while adding user forensic notification details in DB. Reason :{}", e.getMessage(), e);
//            throw new ControlCenterException("Error while adding user forensic notification details in DB");
//        } finally {
//            closeDaoConnection(handle, signalNotificationPreferenceDao);
//        }
//    }
//
//
//    public static void updateNotifications(List<NotificationBean> notificationBean) throws ControlCenterException {
//
//        SignalNotificationPreferenceDao signalNotificationPreferenceDao = null;
//        try {
//            signalNotificationPreferenceDao =
//                    MySQLConnectionManager.getInstance().getHandle().open(SignalNotificationPreferenceDao.class);
//            signalNotificationPreferenceDao.updateNotifications(notificationBean);
//        } catch (Exception e) {
//            logger.error("Error while updating user notification mapping from DB. Reason :{}", e.getMessage(), e);
//            throw new ControlCenterException("Error while updating user notification mapping from DB");
//        } finally {
//            if (signalNotificationPreferenceDao != null) {
//                MySQLConnectionManager.getInstance().close(signalNotificationPreferenceDao);
//            }
//        }
//    }
//
//    public static int getNotificationPreferencesForUser(int signalTypeId, int severityTypeId, int applicationId, String applicableUserId, int accountId, Handle handle) {
//        SignalNotificationPreferenceDao userNotificationDao = getSignalNotifPreferenceDao(handle);
//        try {
//            return userNotificationDao.getNotificationPreferencesForUser(signalTypeId, severityTypeId, applicationId, applicableUserId, accountId);
//        } catch (Exception e) {
//            logger.error("Error in getting user application details from DB. application id:{}", applicationId, e);
//        } finally {
//            closeDaoConnection(handle, userNotificationDao);
//        }
//
//        return -1;
//    }
//
//    public static int addNotificationUserDetails(UserNotificationDetailsBean userNotificationDetailsBean, Handle handle) throws ControlCenterException {
//        SignalNotificationPreferenceDao signalNotificationPreferenceDao = getSignalNotifPreferenceDao(handle);
//        try {
//            return signalNotificationPreferenceDao.addNotificationUserDetails(userNotificationDetailsBean);
//        } catch (Exception e) {
//            logger.error("Error while inserting user notification details from DB. Reason :{}", e.getMessage(), e);
//            throw new ControlCenterException("Error while inserting user notification details from DB");
//        } finally {
//            closeDaoConnection(handle, signalNotificationPreferenceDao);
//        }
//    }
//
//    public static void updateNotificationDetailsForUsers(List<UserNotificationDetailsBean> list, Handle handle) throws ControlCenterException {
//        SignalNotificationPreferenceDao signalNotificationPreferenceDao = getSignalNotifPreferenceDao(handle);
//        try {
//            signalNotificationPreferenceDao.updateNotificationDetailsForUsers(list);
//        } catch (Exception e) {
//            logger.error("Error while updating user notification details. Reason :{}", e.getMessage(), e);
//            throw new ControlCenterException("Error while updating user notification details");
//        } finally {
//            closeDaoConnection(handle, signalNotificationPreferenceDao);
//        }
//    }
//
//    public static void updateNotificationUserDetails(boolean smsEnabled, boolean emailEnabled, String user, Date updatedTime) throws ControlCenterException {
//
//        SignalNotificationPreferenceDao signalNotificationPreferenceDao = null;
//        try {
//            signalNotificationPreferenceDao =
//                    MySQLConnectionManager.getInstance().getHandle().open(SignalNotificationPreferenceDao.class);
//            signalNotificationPreferenceDao.updateNotificationUserDetails(smsEnabled, emailEnabled, user, updatedTime);
//        } catch (Exception e) {
//            logger.error("Error while update user notification details from DB. Reason :{}", e.getMessage(), e);
//            throw new ControlCenterException("Error while updating user notification details from DB");
//        } finally {
//            if (signalNotificationPreferenceDao != null) {
//                MySQLConnectionManager.getInstance().close(signalNotificationPreferenceDao);
//            }
//        }
//    }

//    public static List<UserNotificationDetails> getEmailSmsForensicNotificationStatusForUsers(Handle handle) {
//        SignalNotificationPreferenceDao notifPreferenceDao = getSignalNotifPreferenceDao(handle);
//        try {
//            return notifPreferenceDao.getEmailSmsForensicNotificationStatusForUsers();
//        } catch (Exception e) {
//            logger.error("Exception encountered while fetching user notification details. Reason: {}", e.getMessage(), e);
//            return null;
//        } finally {
//            closeDaoConnection(handle, notifPreferenceDao);
//        }
//    }
//
//    private static SignalNotificationPreferenceDao getSignalNotifPreferenceDao(Handle handle) {
//        if (handle == null) {
//            return MySQLConnectionManager.getInstance().open(SignalNotificationPreferenceDao.class);
//        } else {
//            return handle.attach(SignalNotificationPreferenceDao.class);
//        }
//    }
//
//    private static void closeDaoConnection(Handle handle, SignalNotificationPreferenceDao dao) {
//        if (handle == null) {
//            MySQLConnectionManager.getInstance().close(dao);
//        }
//    }
}
