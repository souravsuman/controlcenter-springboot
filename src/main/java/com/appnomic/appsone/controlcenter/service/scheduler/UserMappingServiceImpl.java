package com.appnomic.appsone.controlcenter.service.scheduler;

import com.appnomic.appsone.controlcenter.dao.mysql.UserAccessDataDao;
import com.appnomic.appsone.controlcenter.dao.mysql.UserDataDao;
import com.appnomic.appsone.controlcenter.exceptions.ControlCenterException;
import com.appnomic.appsone.controlcenter.service.NotificationPreferencesDataService;
import org.apache.logging.log4j.core.util.Throwables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Component
public class UserMappingServiceImpl {

    @Autowired
    UserAccessDataDao userAccessDao;
    @Autowired
    NotificationPreferencesDataService notificationPreferencesDataService;
    @Autowired
    UserDataDao userDataDao;

    private static final Logger logger = LoggerFactory.getLogger(UserMappingServiceImpl.class);

    @Scheduled(initialDelay = 1000, fixedRate = 10000)
    public  void runOneIteration(){
        try {
            List<String> userDetails = userAccessDao.getUserIdentifiers();
            logger.info("called");
            if (!userDetails.isEmpty()) {
                List<String> keycloakUserIds = userAccessDao.getKeycloakUserIdentifiers();
                for (String userId : userDetails) {
                    if (keycloakUserIds == null || !keycloakUserIds.contains(userId)) {
                        logger.info("User [{}] exists in appsone, but unavailable in keycloak", userId);
                        deleteData(userId);
                        logger.info("User [{}] deleted from appsone as this user is unavailable in keycloak", userId);
                    }
                }
            }
        } catch (ControlCenterException e) {
            logger.error("Error occurred while user mapping scheduler execute", e);
        }
    }

    @Transactional
    private void deleteData(String userId) throws ControlCenterException {
        try {
            notificationPreferencesDataService.removeNotificationDetailsForUser(userId);
            notificationPreferencesDataService.removeUserNotificationPreferencesForUser(userId);
            notificationPreferencesDataService.removeForensicNotificationPreferencesForUser(userId);
            userDataDao.deleteUserAttributesAndAccessDetails(userId);
        } catch (Exception e) {
            logger.error("Exception encountered when deleting redundant users in scheduler. Details: {}", e.getMessage(), e);
            throw new ControlCenterException(Throwables.getRootCause(e).getMessage());
        }
    }
}
